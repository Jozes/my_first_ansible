**Clone repo locally**

```your_computer~:``` git clone https://gitlab.com/djieno/my_first_ansible.git

**Run ansible site.yml**

```your_computer/my_first_ansible~:``` ansible-playbook site.yml 

**Run specific tag against specific host**

```your_computer/my_first_ansible~:``` ansible-playbook site.yml -l some_specific_host --tags=some_tag_only









